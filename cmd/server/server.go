package main

import (
	"crypto/tls"
	"gitlab.com/Icon_ka/tcp/handlers"
	"gitlab.com/Icon_ka/tcp/serverx"
	"log"
	"time"
)

const (
	protected = iota
	unprotected
)

func main() {
	quotes := handlers.NewQuotes("static/quotes.dat")
	routes := map[int]serverx.HandlerFunc{
		protected:   quotes.ProtectedRandomQuotes,
		unprotected: quotes.UnprotectedRandomQuotes,
	}

	cert, err := tls.LoadX509KeyPair("./etc/ssl/certs/server.crt", "./etc/ssl/private/server.key")
	if err != nil {
		log.Fatalf("server: loadkeys: %s", err)
	}

	tlsConfig := &tls.Config{Certificates: []tls.Certificate{cert}}

	server, err := serverx.NewServer(":8989", 5*time.Minute, routes, tlsConfig)
	if err != nil {
		log.Fatalf("Failed to start server: %v", err)
	}
	log.Println("Server started")
	server.Start()
}
