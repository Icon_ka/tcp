package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"gitlab.com/Icon_ka/tcp/handlers"
	"gitlab.com/Icon_ka/tcp/serverx"
	"log"
	"math/big"
	"testing"
	"time"
)

const (
	protected = iota
	unprotected
)

func TestM(t *testing.T) {
	quotes := handlers.NewQuotes("../../static/quotes.dat")
	routes := map[int]serverx.HandlerFunc{
		protected:   quotes.ProtectedRandomQuotes,
		unprotected: quotes.UnprotectedRandomQuotes,
	}

	certPEM, keyPEM, err := generateSelfSignedCert()
	if err != nil {
		t.Fatal(err)
	}

	cert, err := tls.X509KeyPair(certPEM, keyPEM)
	if err != nil {
		t.Fatal(err)
	}

	tlsConfig := &tls.Config{Certificates: []tls.Certificate{cert}}

	server, err := serverx.NewServer(":8989", 5*time.Minute, routes, tlsConfig)
	if err != nil {
		log.Fatalf("Failed to start server: %v", err)
	}
	log.Println("Server started")
	go server.Start()

	main()
}

func generateSelfSignedCert() ([]byte, []byte, error) {
	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, nil, err
	}

	template := x509.Certificate{
		SerialNumber: big.NewInt(1),
		Subject: pkix.Name{
			Organization: []string{"Testing Organization"},
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().Add(time.Hour * 24 * 365),
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &priv.PublicKey, priv)
	if err != nil {
		return nil, nil, err
	}

	cert := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	key := pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(priv)})

	return cert, key, nil
}
