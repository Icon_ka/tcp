package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"gitlab.com/Icon_ka/tcp/clientx"
	"gitlab.com/Icon_ka/tcp/message"
	"gitlab.com/Icon_ka/tcp/powx"
	"log"
	"math"
	"sync"
	"time"
)

// main don't look at magic numbers, they are used to test the server :D
func main() {
	client, err := clientx.NewClient("localhost:8989", &tls.Config{InsecureSkipVerify: true})
	if err != nil {
		log.Fatalf("Failed to create client: %v \n", err)
	}
	log.Println("Starting client without PoW protection")
	time.Sleep(1 * time.Second)
	var mu sync.Mutex
	var maxNoPowRPS int
	var wg sync.WaitGroup
	for i := 1; i <= 15; i++ {
		wg.Add(1)
		var response message.Response
		go func(id int) {
			defer wg.Done()
			id = id * 100_000_000
			ctx, cancel := context.WithTimeout(context.Background(), 1*time.Millisecond)
			req := message.Request{
				ID:   id,
				Path: 1,
			}
			response, _ = client.Request(ctx, req)
			cancel()
			for j := 0; j < 5_000; j++ {
				var reqErr error
				req.Hmac = response.HMAC
				req.Pow = response.Pow
				var nonce int
				if response.HMAC != nil {
					for i := 0; i < 1000000; i++ {
						if powx.VerifySolution(response.Pow.Task, fmt.Sprintf("%d", i)) {
							nonce = i
							break
						}
					}
				}
				req.Nonce = nonce
				req.ID = j + id
				ctx, cancel = context.WithTimeout(context.Background(), 1*time.Millisecond)
				response, reqErr = client.Request(ctx, req)
				cancel()

				if reqErr != nil {
					log.Printf("Failed to get response for message %d: %v", 0, reqErr)
				}

				if response.RPS > maxNoPowRPS {
					mu.Lock()
					maxNoPowRPS = response.RPS
					mu.Unlock()
				}
				if response.StatusCode != message.OK {
					log.Printf("Received error response %+v", response)
				}

				if response.RPS > 8_000 {
					log.Printf("Received response: %+v", response)
				}
			}
		}(i)
	}
	wg.Wait()

	log.Printf("End unprotected quotes test, max RPS: %d\n", maxNoPowRPS)
	log.Printf("Wait starting PoW client\n")

	time.Sleep(40 * time.Second)

	maxRPS := 0
	var maxDifficult string
	var maxDiffMu sync.Mutex

	var minRPS = math.MaxInt
	var minRPSMu sync.Mutex

	log.Println("Starting PoW client")
	for i := 1; i <= 15; i++ {
		wg.Add(1)
		var response message.Response
		go func(id int) {
			defer wg.Done()
			id = id * 100_000_0000
			ctx, cancel := context.WithTimeout(context.Background(), 1*time.Millisecond)
			req := message.Request{
				ID: id,
			}
			response, _ = client.Request(ctx, req)
			cancel()
			for j := 0; j < 3000; j++ {
				var reqErr error
				req.Hmac = response.HMAC
				req.Pow = response.Pow
				var nonce int
				if response.HMAC != nil {
					for i := 0; i < 10_000_000; i++ {
						if powx.VerifySolution(response.Pow.Task, fmt.Sprintf("%d", i)) {
							nonce = i
							break
						}
					}
				}
				req.Nonce = nonce
				req.ID = j + id
				ctx, cancel = context.WithTimeout(context.Background(), 1*time.Millisecond)
				response, reqErr = client.Request(ctx, req)
				cancel()

				if reqErr != nil {
					log.Printf("Failed to get response for message %d: %v", 0, reqErr)
				}

				if response.StatusCode != message.OK {
					log.Printf("Received error response %+v", response)
				}
				if response.RPS > maxRPS {
					mu.Lock()
					maxRPS = response.RPS
					mu.Unlock()
				}

				if response.RPS < minRPS {
					minRPSMu.Lock()
					minRPS = response.RPS
					minRPSMu.Unlock()
				}

				if len(response.Pow.Task.Difficulty) > len(maxDifficult) {
					maxDiffMu.Lock()
					maxDifficult = response.Pow.Task.Difficulty
					maxDiffMu.Unlock()
				}

				if response.RPS > 500 {
					log.Printf("Received response: %+v", response)
				}
			}
		}(i)
	}
	wg.Wait()
	log.Printf("max no PoW RPS: %d\n", maxNoPowRPS)
	log.Printf("End PoW quotes test, max RPS: %d, min RPS: %d, max difficulty: %s\n", maxRPS, minRPS, maxDifficult)
	client.Close()
}
