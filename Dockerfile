# Build stage
FROM golang:1.19.7-alpine AS build
WORKDIR /go/src/gitlab.com/Icon_ka/tcp

COPY go.mod go.sum ./
RUN go mod download && apk add --no-cache ca-certificates

COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-s -w -extldflags "-static"' -o /app ./cmd/server/

# Final stage
FROM scratch
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /go/src/gitlab.com/Icon_ka/tcp/etc/ssl/certs/server.crt /etc/ssl/certs/
COPY --from=build /go/src/gitlab.com/Icon_ka/tcp/etc/ssl/private/server.key /etc/ssl/private/
COPY --from=build /go/src/gitlab.com/Icon_ka/tcp/static /static
COPY --from=build /app /app

EXPOSE 8989

CMD ["/app"]
