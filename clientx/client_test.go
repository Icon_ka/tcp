package clientx

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/gob"
	"encoding/pem"
	"fmt"
	"gitlab.com/Icon_ka/tcp/message"
	"math/big"
	"net"
	"reflect"
	"testing"
	"time"
)

func TestClient_Request(t *testing.T) {
	type args struct {
		ctx context.Context
		msg message.Request
	}
	tests := []struct {
		name       string
		responseCh map[int]chan message.Response
		args       args
		want       message.Response
		wantErr    bool
	}{
		{
			name:       "Request",
			responseCh: make(map[int]chan message.Response),
			args: args{
				ctx: context.Background(),
				msg: message.Request{
					ID: 123,
				},
			},
			want: message.Response{
				ID: 123,
			},
			wantErr: false,
		},
		{
			name:       "Request ctx done",
			responseCh: make(map[int]chan message.Response),
			args: args{
				ctx: getCanceledContext(),
				msg: message.Request{
					ID: 123,
				},
			},
			want:    message.Response{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			in, out := net.Pipe()
			enc := gob.NewEncoder(in)
			c := &Client{
				responseCh: tt.responseCh,
				enc:        enc,
			}

			go func() {
				tmp := &message.Response{}
				if err := gob.NewDecoder(out).Decode(tmp); err != nil {
					t.Error(err)
				}

				tt.responseCh[tt.args.msg.ID] <- *tmp
			}()

			got, err := c.Request(tt.args.ctx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("Request() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Request() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func getCanceledContext() context.Context {
	res, cancel := context.WithCancel(context.Background())
	cancel()
	return res
}

func TestClient_listen(t *testing.T) {
	tests := []struct {
		name       string
		responseCh map[int]chan message.Response
		response   message.Response
	}{
		{
			name:       "listen",
			responseCh: make(map[int]chan message.Response),
			response: message.Response{
				ID: 123,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ch := make(chan message.Response)
			tt.responseCh[tt.response.ID] = ch

			in, out := net.Pipe()
			dec := gob.NewDecoder(in)
			c := &Client{
				responseCh: tt.responseCh,
				dec:        dec,
			}
			go c.listen()

			if err := gob.NewEncoder(out).Encode(tt.response); err != nil {
				t.Error(err)
			}

			got := <-ch
			if !reflect.DeepEqual(got, tt.response) {
				t.Errorf("listen got response: %v, want %v", got, tt.response)
			}
			c.mu.Lock()
			defer c.mu.Unlock()
			if _, ok := tt.responseCh[tt.response.ID]; ok {
				t.Errorf("listen response was not deleted")
			}
		})
	}
}

func TestNewClient(t *testing.T) {
	tests := []struct {
		name       string
		serverAddr string
		wantErr    bool
	}{
		{
			name:       "NewClient",
			serverAddr: "localhost:3030",
			wantErr:    false,
		},
		{
			name:       "NewClient",
			serverAddr: "123",
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			certPEM, keyPEM, err := generateSelfSignedCert()
			if err != nil {
				t.Fatal(err)
			}

			cert, err := tls.X509KeyPair(certPEM, keyPEM)
			if err != nil {
				t.Fatal(err)
			}

			l, err := tls.Listen("tcp", ":3030", &tls.Config{
				Certificates: []tls.Certificate{cert},
			})

			go func() {
				for {
					conn, err := l.Accept()
					if err != nil {
						return
					}
					fmt.Fprintf(conn, "Hello, client!")
					conn.Close()
				}
			}()

			if err != nil {
				t.Error(err)
				return
			}
			defer l.Close()

			got, err := NewClient(tt.serverAddr, &tls.Config{
				InsecureSkipVerify: true,
			})

			if (err != nil) != tt.wantErr {
				t.Errorf("NewClient() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if tt.wantErr {
				if got != nil {
					t.Errorf("NewClient() got not nil: %v", got)
					return
				}
			} else {
				if got.conn == nil || got.enc == nil || got.dec == nil || got.responseCh == nil {
					t.Error("NewClient() got zero value")
					return
				}
			}
		})
	}
}

func generateSelfSignedCert() ([]byte, []byte, error) {
	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, nil, err
	}

	template := x509.Certificate{
		SerialNumber: big.NewInt(1),
		Subject: pkix.Name{
			Organization: []string{"Testing Organization"},
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().Add(time.Hour * 24 * 365),
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &priv.PublicKey, priv)
	if err != nil {
		return nil, nil, err
	}

	cert := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	key := pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(priv)})

	return cert, key, nil
}

func TestCloseClient(t *testing.T) {
	certPEM, keyPEM, err := generateSelfSignedCert()
	if err != nil {
		t.Fatal(err)
	}

	cert, err := tls.X509KeyPair(certPEM, keyPEM)
	if err != nil {
		t.Fatal(err)
	}

	l, err := tls.Listen("tcp", ":3030", &tls.Config{
		Certificates: []tls.Certificate{cert},
	})

	go func() {
		for {
			conn, err := l.Accept()
			if err != nil {
				return
			}
			fmt.Fprintf(conn, "Hello, client!")
			conn.Close()
		}
	}()

	if err != nil {
		t.Error(err)
		return
	}
	defer l.Close()

	got, err := NewClient(":3030", &tls.Config{
		InsecureSkipVerify: true,
	})

	if err != nil {
		t.Error(err)
		return
	}
	defer got.Close()
}
