package clientx

import (
	"context"
	"crypto/tls"
	"encoding/gob"
	"gitlab.com/Icon_ka/tcp/message"
	"io"
	"log"
	"sync"
)

// Client структура tcp клиента
type Client struct {
	conn       io.ReadWriteCloser
	enc        *gob.Encoder
	dec        *gob.Decoder
	responseCh map[int]chan message.Response
	mu         sync.Mutex
}

// NewClient конструктор tcp клиента
func NewClient(serverAddr string, tlsCFG *tls.Config) (*Client, error) {
	conn, err := tls.Dial("tcp", serverAddr, tlsCFG)
	if err != nil {
		return nil, err
	}
	client := &Client{
		conn:       conn,
		enc:        gob.NewEncoder(conn),
		dec:        gob.NewDecoder(conn),
		responseCh: make(map[int]chan message.Response),
	}
	go client.listen()
	return client, nil
}

// Request метод отправки tcp запроса и получения ответа
func (c *Client) Request(ctx context.Context, msg message.Request) (message.Response, error) {
	responseChan := make(chan message.Response, 1)

	c.mu.Lock()
	c.responseCh[msg.ID] = responseChan
	c.mu.Unlock()

	if err := c.enc.Encode(msg); err != nil {
		return message.Response{}, err
	}

	select {
	case response := <-responseChan:
		return response, nil
	case <-ctx.Done():

		c.mu.Lock()
		if c.responseCh[msg.ID] != nil {
			close(c.responseCh[msg.ID])
			delete(c.responseCh, msg.ID)
		}
		c.mu.Unlock()

		return message.Response{}, ctx.Err()
	}
}

func (c *Client) listen() {
	for {
		var response message.Response
		if err := c.dec.Decode(&response); err != nil {
			log.Printf("Error decoding response: %v", err)
			continue
		}

		c.mu.Lock()
		if ch, ok := c.responseCh[response.ID]; ok {
			ch <- response
			close(ch)
			delete(c.responseCh, response.ID)
		}
		c.mu.Unlock()
	}
}

// Close метод закрытия tcp клиента
func (c *Client) Close() error {
	return c.conn.Close()
}
