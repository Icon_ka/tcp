package powx

import (
	"gitlab.com/ptflp/ddos-pow/pow"
)

// VerifySolution проверяет решение PoW задачи
func VerifySolution(task pow.Task, clientNonce string) bool {
	return pow.VerifyPoWSolution(task.Salt, task.Nonce, clientNonce, task.Difficulty)
}
