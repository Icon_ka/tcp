package serverx

import (
	"context"
	"encoding/gob"
	"errors"
	"log"
	"net"
	"sync"
	"sync/atomic"
	"time"

	"crypto/tls"
	"gitlab.com/Icon_ka/tcp/message"
)

// HandlerFunc обработчик tcp запроса
type HandlerFunc func(req message.Request) message.Response

// Server структура tcp сервера
type Server struct {
	rps               int
	MaxConnections    int
	requestCount      int64
	Address           string
	lastReset         time.Time
	lock              sync.Mutex
	ConnectionTimeout time.Duration
	routes            map[int]HandlerFunc
	tlsCFG            *tls.Config
}

// ErrNoRoutes ошибка отсутствия маршрутов
var ErrNoRoutes = errors.New("no routes")

// NewServer конструктор tcp сервера
func NewServer(address string, connectionTimeout time.Duration, routes map[int]HandlerFunc, config *tls.Config) (*Server, error) {
	if len(routes) == 0 {
		return nil, ErrNoRoutes
	}

	return &Server{
		Address:           address,
		ConnectionTimeout: connectionTimeout,
		routes:            routes,
		lastReset:         time.Now(),
		tlsCFG:            config,
	}, nil
}

func (s *Server) handleConnection(ctx context.Context, conn net.Conn) {
	defer conn.Close()
	err := conn.SetDeadline(time.Now().Add(s.ConnectionTimeout))
	if err != nil {
		log.Printf("Error setting deadline: %v", err)
		return
	}

	dec := gob.NewDecoder(conn)
	enc := gob.NewEncoder(conn)

	for {
		select {
		case <-ctx.Done():
			return
		default:
			var msg message.Request
			if err := dec.Decode(&msg); err != nil {
				log.Printf("Error decoding message: %v", err)
				return
			}
			atomic.AddInt64(&s.requestCount, 1)

			rps := s.rps
			response := message.Response{
				ID:  msg.ID,
				RPS: rps,
			}
			msg.RPS = rps
			handler, ok := s.routes[msg.Path]
			if ok {
				response = handler(msg)
				response.RPS = rps
			} else {
				response.StatusCode = message.NotFound
				log.Printf("No handler for message ID: %d", msg.ID)
			}

			if err := enc.Encode(response); err != nil {
				log.Printf("Error encoding response: %v", err)
				return
			}
		}
	}
}

func (s *Server) calculateRPS(tickRate time.Duration) {
	for range time.Tick(tickRate) {
		s.lock.Lock()

		now := time.Now()
		duration := now.Sub(s.lastReset).Milliseconds()
		s.rps = int(atomic.SwapInt64(&s.requestCount, 0) * 1000 / duration) // low accuracy
		s.lastReset = now

		s.lock.Unlock()
	}
}

// Start метод запуска tcp сервера
func (s *Server) Start() {
	go s.calculateRPS(250 * time.Millisecond)

	listener, err := tls.Listen("tcp", s.Address, s.tlsCFG)
	if err != nil {
		log.Fatalf("Error listening: %v", err)
	}
	defer listener.Close()
	log.Printf("Server is listening on %s", s.Address)

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("Error accepting: %v", err)
			continue
		}

		go func(c net.Conn) {
			ctx, cancel := context.WithTimeout(context.Background(), s.ConnectionTimeout) // prevent idle connections from hanging
			defer cancel()
			s.handleConnection(ctx, c)
		}(conn)
	}
}
