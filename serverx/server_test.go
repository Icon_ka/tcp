package serverx

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/gob"
	"encoding/pem"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/Icon_ka/tcp/message"
	"gitlab.com/ptflp/ddos-pow/pow"
	"gitlab.com/ptflp/ddos-pow/quotes/entity"
	"math/big"
	"net"
	"reflect"
	"testing"
	"time"
)

func TestNewServer(t *testing.T) {
	emptyHandlerFunc := func(req message.Request) message.Response { return message.Response{} }

	type args struct {
		address           string
		connectionTimeout time.Duration
		routes            map[int]HandlerFunc
		tlsCFG            *tls.Config
	}
	tests := []struct {
		name         string
		args         args
		want         *Server
		wantErrValue error
		wantErr      bool
	}{
		{
			name: "NewServer",
			args: args{
				address:           "test_addr",
				connectionTimeout: 321,
				routes:            map[int]HandlerFunc{0: emptyHandlerFunc, 1: emptyHandlerFunc},
				tlsCFG:            &tls.Config{InsecureSkipVerify: true},
			},
			want: &Server{
				Address:           "test_addr",
				ConnectionTimeout: 321,
				routes:            map[int]HandlerFunc{0: emptyHandlerFunc, 1: emptyHandlerFunc},
				tlsCFG:            &tls.Config{InsecureSkipVerify: true},
			},
			wantErr: false,
		},
		{
			name: "NewServer err",
			args: args{
				routes: map[int]HandlerFunc{},
				tlsCFG: &tls.Config{InsecureSkipVerify: true},
			},
			want:         nil,
			wantErrValue: ErrNoRoutes,
			wantErr:      true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewServer(tt.args.address, tt.args.connectionTimeout, tt.args.routes, tt.args.tlsCFG)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewServer() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if tt.wantErr {
				if !errors.Is(err, tt.wantErrValue) {
					t.Errorf("NewServer() error = %v, wantErrValue %v", err, tt.wantErrValue)
				}
				return
			}
			if got.Address != tt.want.Address ||
				got.MaxConnections != tt.want.MaxConnections ||
				got.ConnectionTimeout != tt.want.ConnectionTimeout ||
				len(got.routes) != len(tt.want.routes) {
				t.Errorf("NewServer() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestServer_Start(t *testing.T) {
	certPEM, keyPEM, err := generateSelfSignedCert()
	if err != nil {
		t.Fatal(err)
	}

	cert, err := tls.X509KeyPair(certPEM, keyPEM)
	if err != nil {
		t.Fatal(err)
	}

	s := &Server{
		Address:           ":3000",
		ConnectionTimeout: 1 * time.Second,
		routes:            map[int]HandlerFunc{},
		tlsCFG: &tls.Config{
			InsecureSkipVerify: true,
			Certificates:       []tls.Certificate{cert},
		},
	}

	s.lock.Lock()
	defer s.lock.Unlock()

	go s.Start()

	start := time.Now()

	for {
		_, err := tls.Dial("tcp", "localhost:3000", &tls.Config{InsecureSkipVerify: true})
		if err == nil {
			break // Server is up and running
		}
		if time.Since(start) > time.Second*10 {
			t.Fatal("Server didn't start in time")
		}
		time.Sleep(time.Millisecond * 100)
	}

	conn, err := tls.Dial("tcp", "localhost:3000", &tls.Config{
		InsecureSkipVerify: true,
		RootCAs:            x509.NewCertPool(),
	})
	if err != nil {
		t.Error(err)
		return
	}
	enc := gob.NewEncoder(conn)
	request := message.Request{ID: 123}
	err = enc.Encode(request)
	if err != nil {
		t.Error(err)
		return
	}
	dec := gob.NewDecoder(conn)
	var response message.Response
	if err := dec.Decode(&response); err != nil {
		t.Error(err)
		return
	}
	if response.ID != request.ID && response.StatusCode != message.NotFound {
		t.Errorf("handleConnection() was not called")
	}
}

func TestServer_StartErrors(t *testing.T) {
	certPEM, keyPEM, err := generateSelfSignedCert()
	if err != nil {
		t.Fatal(err)
	}

	cert, err := tls.X509KeyPair(certPEM, keyPEM)
	if err != nil {
		t.Fatal(err)
	}

	s := &Server{
		Address:           ":3010",
		ConnectionTimeout: 1 * time.Second,
		routes:            map[int]HandlerFunc{},
		tlsCFG: &tls.Config{
			InsecureSkipVerify: true,
			Certificates:       []tls.Certificate{cert},
		},
	}

	s.lock.Lock()
	defer s.lock.Unlock()

	go s.Start()

	start := time.Now()

	for {
		_, err := tls.Dial("tcp", "localhost:3010", &tls.Config{InsecureSkipVerify: true})
		if err == nil {
			break // Server is up and running
		}
		if time.Since(start) > time.Second*10 {
			t.Fatal("Server didn't start in time")
		}
		time.Sleep(time.Millisecond * 100)
	}

	conn, err := tls.Dial("tcp", "localhost:3010", &tls.Config{
		InsecureSkipVerify: true,
		RootCAs:            x509.NewCertPool(),
	})
	if err != nil {
		t.Error(err)
		return
	}
	enc := gob.NewEncoder(conn)
	request := message.Request{ID: 123}
	err = enc.Encode(request)
	if err != nil {
		t.Error(err)
		return
	}
	dec := gob.NewDecoder(conn)
	var response message.Response
	if err := dec.Decode(&response); err != nil {
		t.Error(err)
		return
	}
	if response.ID != request.ID && response.StatusCode != message.NotFound {
		t.Errorf("handleConnection() was not called")
	}

	if !assert.Error(t, errors.New("Error listening: listen tcp :3000: bind: address already in use")) {
		t.Errorf("dada")
	}
}

func generateSelfSignedCert() ([]byte, []byte, error) {
	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, nil, err
	}

	template := x509.Certificate{
		SerialNumber: big.NewInt(1),
		Subject: pkix.Name{
			Organization: []string{"Testing Organization"},
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().Add(time.Hour * 24 * 365),
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &priv.PublicKey, priv)
	if err != nil {
		return nil, nil, err
	}

	cert := pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	key := pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(priv)})

	return cert, key, nil
}

func TestServer_calculateRPS(t *testing.T) {
	tests := []struct {
		name         string
		requestCount int64
		tickRate     time.Duration
	}{
		{
			name:         "calculateRPS 0 for 250ms",
			requestCount: 0,
			tickRate:     250 * time.Millisecond,
		},
		{
			name:         "calculateRPS 100 for 250ms",
			requestCount: 100,
			tickRate:     250 * time.Millisecond,
		},
		{
			name:         "calculateRPS 10000 for 250ms",
			requestCount: 10000,
			tickRate:     250 * time.Millisecond,
		},
		{
			name:         "calculateRPS 10000 for 666ms",
			requestCount: 10000,
			tickRate:     666 * time.Millisecond,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			oneTickWait := tt.tickRate + 5*time.Millisecond
			lastReset := time.Now()
			s := &Server{
				requestCount: tt.requestCount,
				lastReset:    lastReset,
			}
			go s.calculateRPS(tt.tickRate)

			time.Sleep(oneTickWait)
			s.lock.Lock()
			defer s.lock.Unlock()

			tmp := tt.requestCount * 1000
			if !isBetween(tmp/oneTickWait.Milliseconds(), tmp/tt.tickRate.Milliseconds(), int64(s.rps)) {
				t.Errorf("calculateRPS() rps is incorrect %d", s.rps)
			}
			if s.requestCount != 0 {
				t.Errorf("calculateRPS() s.requestCount is not zero %d", s.requestCount)
			}
			if !isBetween(lastReset.UnixNano(), lastReset.Add(oneTickWait).UnixNano(), s.lastReset.UnixNano()) {
				t.Errorf("calculateRPS() s.lastReset is not updated correct %d", s.lastReset.UnixNano())
			}
		})
	}
}

func TestServer_handleConnection(t *testing.T) {
	type fields struct {
		rps               int
		MaxConnections    int
		requestCount      int64
		Address           string
		lastReset         time.Time
		ConnectionTimeout time.Duration
		routes            map[int]HandlerFunc
	}
	tests := []struct {
		name         string
		fields       fields
		request      message.Request
		wantResponse *message.Response
	}{
		{
			name: "handleConnection",
			fields: fields{
				ConnectionTimeout: 1 * time.Second,
				requestCount:      0,
				rps:               1234,
				routes: map[int]HandlerFunc{0: func(req message.Request) message.Response {
					return message.Response{
						ID:         123,
						StatusCode: message.OK,
						Data: entity.QuoteDTO{
							ID:     99,
							Text:   "test_text",
							Author: "test_author",
						},
						Pow: pow.TaskSignature{
							Task: pow.Task{
								Salt:       "test_salt",
								Nonce:      "test_nonce",
								Difficulty: "4",
							},
						},
						HMAC: []byte("test_hmac"),
					}
				}},
			},
			request: message.Request{
				ID:    123,
				Path:  0,
				Hmac:  nil,
				Pow:   pow.TaskSignature{},
				Nonce: 0,
				RPS:   0,
			},
			wantResponse: &message.Response{
				ID:         123,
				StatusCode: message.OK,
				Data: entity.QuoteDTO{
					ID:     99,
					Text:   "test_text",
					Author: "test_author",
				},
				Pow: pow.TaskSignature{
					Task: pow.Task{
						Salt:       "test_salt",
						Nonce:      "test_nonce",
						Difficulty: "4",
					},
				},
				HMAC: []byte("test_hmac"),
				RPS:  1234,
			},
		},
		{
			name: "handleConnection no handler",
			fields: fields{
				ConnectionTimeout: 1 * time.Second,
				requestCount:      0,
				rps:               1234,
				routes:            map[int]HandlerFunc{},
			},
			request: message.Request{
				ID:    123,
				Path:  0,
				Hmac:  nil,
				Pow:   pow.TaskSignature{},
				Nonce: 0,
				RPS:   0,
			},
			wantResponse: &message.Response{
				ID:         123,
				StatusCode: message.NotFound,
				RPS:        1234,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			lastRequestCount := tt.fields.requestCount
			s := &Server{
				rps:               tt.fields.rps,
				MaxConnections:    tt.fields.MaxConnections,
				requestCount:      tt.fields.requestCount,
				Address:           tt.fields.Address,
				lastReset:         tt.fields.lastReset,
				ConnectionTimeout: tt.fields.ConnectionTimeout,
				routes:            tt.fields.routes,
			}
			in, out := net.Pipe()
			go s.handleConnection(context.Background(), in)

			if err := gob.NewEncoder(out).Encode(tt.request); err != nil {
				t.Error(err)
				return
			}

			response := &message.Response{}
			if err := gob.NewDecoder(out).Decode(response); err != nil {
				t.Error(err)
				return
			}

			if !reflect.DeepEqual(response, tt.wantResponse) {
				t.Errorf("handleConnection() response got %v, want %v", response, tt.wantResponse)
			}

			if s.requestCount != lastRequestCount+1 {
				t.Errorf("handleConnection() s.requestCount was not incremented: %d", s.requestCount)
			}
		})
	}
}

func TestServerhandleConnection(t *testing.T) {
	s := &Server{
		rps:               1234,
		MaxConnections:    3,
		requestCount:      10,
		Address:           ":3030",
		ConnectionTimeout: 1 * time.Second,
		routes: map[int]HandlerFunc{0: func(req message.Request) message.Response {
			return message.Response{
				ID:         123,
				StatusCode: message.OK,
				Data: entity.QuoteDTO{
					ID:     99,
					Text:   "test_text",
					Author: "test_author",
				},
				Pow: pow.TaskSignature{
					Task: pow.Task{
						Salt:       "test_salt",
						Nonce:      "test_nonce",
						Difficulty: "4",
					},
				},
				HMAC: []byte("test_hmac"),
			}
		}},
	}
	in, _ := net.Pipe()
	go s.handleConnection(getCanceledContext(), in)

}

func getCanceledContext() context.Context {
	res, cancel := context.WithCancel(context.Background())
	cancel()
	return res
}

func isBetween(l, r, v int64) bool {
	return l <= v && v <= r
}

type MockConn struct {
	mock.Mock
}

func (m *MockConn) Read(b []byte) (n int, err error) {
	args := m.Called(b)
	return args.Int(0), args.Error(1)
}

func (m *MockConn) Write(b []byte) (n int, err error) {
	args := m.Called(b)
	return args.Int(0), args.Error(1)
}

func (m *MockConn) Close() error {
	args := m.Called()
	return args.Error(0)
}

func (m *MockConn) LocalAddr() net.Addr {
	args := m.Called()
	return args.Get(0).(net.Addr)
}

func (m *MockConn) RemoteAddr() net.Addr {
	args := m.Called()
	return args.Get(0).(net.Addr)
}

func (m *MockConn) SetDeadline(t time.Time) error {
	args := m.Called(t)
	return args.Error(0)
}

func (m *MockConn) SetReadDeadline(t time.Time) error {
	args := m.Called(t)
	return args.Error(0)
}

func (m *MockConn) SetWriteDeadline(t time.Time) error {
	args := m.Called(t)
	return args.Error(0)
}

func TestServer_HandleConnection_SetDeadlineError(t *testing.T) {
	mockConn := new(MockConn)
	mockConn.On("SetDeadline", mock.Anything).Return(assert.AnError)
	mockConn.On("Close").Return(nil)
	s := &Server{
		ConnectionTimeout: 1 * time.Second,
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	s.handleConnection(ctx, mockConn)
}
