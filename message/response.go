package message

import (
	"gitlab.com/ptflp/ddos-pow/pow"
	"gitlab.com/ptflp/ddos-pow/quotes/entity"
)

const (
	// Unknown неизвестный статус код (пока не используется)
	Unknown StatusCode = iota
	// OK успешный запрос
	OK
	// NotFound запрашиваемый ресурс не найден
	NotFound
	// InternalError некорректный запрос создал ошибку на сервере
	InternalError
)

// StatusCode статус код ответа tcp запроса
type StatusCode int

// Response структура tcp ответа
type Response struct {
	ID         int
	StatusCode StatusCode
	Data       entity.QuoteDTO
	Pow        pow.TaskSignature
	HMAC       []byte
	RPS        int
}
