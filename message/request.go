package message

import (
	"gitlab.com/ptflp/ddos-pow/pow"
)

// Request структура tcp запроса
// ID - идентификатор запроса/пакета, для мультиплексирования
// Path - путь запроса
// HMAC - хэш сообщения
// Pow - PoW задача
// Nonce - решение PoW задачи
// RPS - количество запросов в секунду
type Request struct {
	ID    int
	Path  int
	Hmac  []byte
	Pow   pow.TaskSignature
	Nonce int
	RPS   int
}
