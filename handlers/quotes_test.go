package handlers

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/gob"
	"gitlab.com/Icon_ka/tcp/message"
	"gitlab.com/ptflp/ddos-pow/pow"
	"gitlab.com/ptflp/ddos-pow/quotes/entity"
	"math/rand"
	"reflect"
	"testing"
	"time"
	"unsafe"
)

func TestQuotes_ProtectedRandomQuotes(t *testing.T) {
	type args struct {
		req message.Request
	}
	now := time.Now()
	tests := []struct {
		name string
		args args
		want message.Response
	}{
		{
			name: "ProtectedRandomQuotes",
			args: args{
				req: message.Request{
					ID:   123,
					Hmac: []byte{252, 154, 195, 24, 179, 254, 33, 191, 152, 38, 206, 55, 5, 100, 239, 148, 186, 118, 172, 126, 131, 11, 164, 10, 242, 131, 193, 154, 239, 131, 246, 174},
					Pow: pow.TaskSignature{
						Task: pow.Task{
							Salt:       "uhFAuYTUWggrCGdl",
							Nonce:      "2641",
							Difficulty: "0",
						},
						Timestamp: now,
					},
					Nonce: 7,
				},
			},
			want: message.Response{
				ID:         123,
				StatusCode: message.OK,
				HMAC:       []byte{252, 154, 195, 24, 179, 254, 33, 191, 152, 38, 206, 55, 5, 100, 239, 148, 186, 118, 172, 126, 131, 11, 164, 10, 242, 131, 193, 154, 239, 131, 246, 174},
			},
		},
		{
			name: "ProtectedRandomQuotes HMAC check failed",
			args: args{
				req: message.Request{
					ID:   123,
					Hmac: nil,
				},
			},
			want: message.Response{
				ID:         123,
				StatusCode: message.InternalError,
			},
		},
		{
			name: "ProtectedRandomQuotes PoW check failed",
			args: args{
				req: message.Request{
					ID:   123,
					Hmac: []byte{252, 154, 195, 24, 179, 254, 33, 191, 152, 38, 206, 55, 5, 100, 239, 148, 186, 118, 172, 126, 131, 11, 164, 10, 242, 131, 193, 154, 239, 131, 246, 174},
					Pow: pow.TaskSignature{
						Task: pow.Task{
							Salt:       "uhFAuYTUWggrCGdl",
							Nonce:      "2641",
							Difficulty: "0",
						},
					},
					Nonce: 0,
				},
			},
			want: message.Response{
				ID:         123,
				StatusCode: message.InternalError,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := NewQuotes("../static/quotes.dat")

			if tt.want.StatusCode == message.OK {
				var b bytes.Buffer
				_ = gob.NewEncoder(&b).Encode(tt.args.req.Pow)

				// fix hmac has timezone
				h := hmac.New(sha256.New, []byte(secret))
				h.Write(b.Bytes())
				hmacSig := h.Sum(nil)
				tt.want.HMAC = hmacSig
				tt.args.req.Hmac = hmacSig
			}

			got := q.ProtectedRandomQuotes(tt.args.req)

			if got.ID != tt.want.ID || got.StatusCode != tt.want.StatusCode {
				t.Errorf("ProtectedRandomQuotes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQuotes_UnprotectedRandomQuotes(t *testing.T) {
	type args struct {
		req message.Request
	}
	tests := []struct {
		name string
		args args
		want message.Response
	}{
		{
			name: "UnprotectedRandomQuotes 1",
			args: args{
				req: message.Request{
					ID: 123,
				},
			},
			want: message.Response{
				ID:         123,
				StatusCode: message.OK,
			},
		},
		{
			name: "UnprotectedRandomQuotes 2",
			args: args{
				req: message.Request{
					ID: 321,
				},
			},
			want: message.Response{
				ID:         321,
				StatusCode: message.OK,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := NewQuotes("../static/quotes.dat")
			got := q.UnprotectedRandomQuotes(tt.args.req)
			tt.want.Data = got.Data

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UnprotectedRandomQuotes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_checkHMAC(t *testing.T) {
	type args struct {
		req       message.Request
		secretKey string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "checkHMAC req.Hmac is nil",
			args: args{
				req: message.Request{
					Hmac: nil,
				},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := checkHMAC(tt.args.req, tt.args.secretKey); got != tt.want {
				t.Errorf("checkHMAC() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_loadGob(t *testing.T) {
	tests := []struct {
		name    string
		path    string
		wantLen int
		panic   bool
	}{
		{
			name:    "loadGob",
			path:    "../static/quotes.dat",
			wantLen: maxQuotes,
			panic:   false,
		},
		{
			name:    "loadGob",
			path:    "static/quotes.dat",
			wantLen: maxQuotes,
			panic:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.panic {
				defer func() {
					if r := recover(); r == nil {
						t.Errorf("The code did not panic")
					}
				}()
			}

			if got := loadGob(tt.path); len(got) != tt.wantLen {
				t.Errorf("loadGob() len got = %d, want %d", len(got), tt.wantLen)
			}
		})
	}
}

func Test_calculateDifficult(t *testing.T) {
	tests := []struct {
		name string
		rps  int
		want int
	}{
		{
			name: "calculateDifficult 0",
			rps:  0,
			want: 1,
		},
		{
			name: "calculateDifficult 500",
			rps:  500,
			want: 2,
		},
		{
			name: "calculateDifficult if rps/500 >= 4",
			rps:  500 * 4,
			want: 5,
		},
		{
			name: "calculateDifficult if rps/500 >= 4",
			rps:  500 * 5,
			want: 5,
		},
		{
			name: "calculateDifficult if rps/500 >= 4",
			rps:  500 * 100,
			want: 5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := calculateDifficult(tt.rps); got != tt.want {
				t.Errorf("calculateDifficult() = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkGetRandomQuote(b *testing.B) {
	q := NewQuotes("../static/quotes.dat")
	benchmarks := []struct {
		name string
		do   func()
	}{
		{
			name: "unsafe",
			do: func() {
				_ = q.getRandomQuoteUnsafe()
			},
		},
		{
			name: "slice",
			do: func() {
				_ = q.getRandomQuote()
			},
		},
	}
	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				bm.do()
			}
		})
	}
}

func (q *Quotes) getRandomQuoteUnsafe() entity.QuoteDTO {
	x := uintptr(rand.Intn(maxQuotes))
	quote := *(*entity.QuoteDTO)(unsafe.Pointer(q.data + lockedSize*x)) // nolint: govet // unsafe.Pointer is used for direct memory access
	return quote
}
