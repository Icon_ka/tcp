//nolint:all
package handlers

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/gob"
	"fmt"
	"gitlab.com/Icon_ka/tcp/message"
	"gitlab.com/Icon_ka/tcp/powx"
	"gitlab.com/ptflp/ddos-pow/pow"
	"gitlab.com/ptflp/ddos-pow/quotes/entity"
	"math/rand"
	"os"
	"reflect"
	"time"
	"unsafe"
)

// Quotes структура для обработки tcp запросов
// data - указатель на данные, не используется в виду медленного получения данных через unsafe.Pointer (для демонстрации)
// в тесте присутствует бенчмарк, который показывает, что получение данных через указатель на 5% медленнее
type Quotes struct {
	Quotes []entity.QuoteDTO
	data   uintptr
}

const lockedSize uintptr = 40
const maxQuotes = 1617
const secret = "secret"

// NewQuotes конструктор Quotes tcp handler
func NewQuotes(path string) *Quotes {
	qs := loadGob(path)
	sh := *(*reflect.SliceHeader)(unsafe.Pointer(&qs))
	return &Quotes{
		Quotes: qs,
		data:   sh.Data,
	}
}

// ProtectedRandomQuotes метод хэндлера для получения случайной цитаты с PoW и HMAC
func (q *Quotes) ProtectedRandomQuotes(req message.Request) message.Response {
	powData := pow.TaskSignature{
		Task:      pow.GenerateTask(calculateDifficult(req.RPS), pow.DefaultSaltLength),
		Timestamp: time.Now(),
	}
	var b bytes.Buffer
	_ = gob.NewEncoder(&b).Encode(powData)

	// вычисляем подпись
	h := hmac.New(sha256.New, []byte(secret))
	h.Write(b.Bytes())
	hmacSig := h.Sum(nil)
	// Проверка подписи
	if !checkHMAC(req, secret) {
		return message.Response{
			ID:         req.ID,
			StatusCode: message.InternalError,
			Pow:        powData,
			HMAC:       hmacSig,
		}
	}

	if !powx.VerifySolution(req.Pow.Task, fmt.Sprintf("%d", req.Nonce)) {
		return message.Response{
			ID:         req.ID,
			StatusCode: message.InternalError,
			Pow:        powData,
			HMAC:       hmacSig,
		}
	}

	return message.Response{
		ID:         req.ID,
		StatusCode: message.OK,
		Data:       q.getRandomQuote(),
		Pow:        powData,
		HMAC:       hmacSig,
	}
}

// UnprotectedRandomQuotes метод хэндлера для получения случайной цитаты без PoW и HMAC
func (q *Quotes) UnprotectedRandomQuotes(req message.Request) message.Response {
	return message.Response{
		ID:         req.ID,
		StatusCode: message.OK,
		Data:       q.getRandomQuote(),
	}
}

func checkHMAC(req message.Request, secretKey string) bool {
	if req.Hmac == nil {
		return false
	}
	var b bytes.Buffer
	_ = gob.NewEncoder(&b).Encode(req.Pow)

	// вычисляем подпись
	h := hmac.New(sha256.New, []byte(secretKey))
	h.Write(b.Bytes())
	hmacSig := h.Sum(nil)

	return hmac.Equal(hmacSig, req.Hmac)
}

func (q *Quotes) getRandomQuote() entity.QuoteDTO {
	return q.Quotes[rand.Intn(len(q.Quotes))]
}

// func to load gob file []entity.QuoteDTO
func loadGob(path string) []entity.QuoteDTO {
	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	dec := gob.NewDecoder(f)
	var quotes []entity.QuoteDTO
	err = dec.Decode(&quotes)
	if err != nil {
		panic(err)
	}

	return quotes
}

func calculateDifficult(rps int) int {
	d := 1 + rps/500
	if d > 5 {
		d = 5
	}

	return d
}
